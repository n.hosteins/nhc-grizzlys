package com.amiltone.grizzly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

@SpringBootApplication
public class GrizzlyApplication {

	public static void main(String[] args) {

		ArrayList<String> list = new ArrayList<String>();
		list.add("./uploads/news");
		list.add("./uploads/teams");
		list.add("./uploads/contact");
		list.add("./uploads/partners");
		list.add("./uploads/asso");
		Iterator iter = list.iterator();
		while (iter.hasNext()) {
			File file = new File((String) iter.next());
			if (file.mkdirs()) {
				System.out.println("Directory is created!");
			}
		}
		SpringApplication.run(GrizzlyApplication.class, args);
	}


}
