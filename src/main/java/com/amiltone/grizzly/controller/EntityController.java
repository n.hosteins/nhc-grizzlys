package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.response.ResponseStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public abstract class EntityController<E> {
    // Méthodes à implémenter dans les classes filles
    protected abstract E getEntity(final long id);

    protected abstract String deleteEntity(final E entity);

    // Méthodes finales
    protected final ResponseEntity<ResponseStatus> getDeleteResponseStatus(final long id) {
        final E entity = getEntity(id);
        if (entity == null) {
            return ResponseEntity.ok(new ResponseStatus("success" , "L'entité est déjà supprimée"));
        }
        final String message = deleteEntity(entity);
        return ResponseEntity.ok(ResponseStatus.success(message));
    }
}
