package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.*;
import com.amiltone.grizzly.holder.DayHolder;
import com.amiltone.grizzly.holder.TrainingHolder;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.TeamsService;
import com.amiltone.grizzly.service.TrainingService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.json.*;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin("*")
@RestController
public class TrainingController extends EntityController<TrainingEntity> {
    @Autowired
    private TrainingService trainingService;

    @Autowired
    private TeamsService teamService;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/getAllTrainings")
    public List<TrainingEntity> getAllTrainings() {
        return trainingService.findAllTraining();
    }

    @GetMapping("/getTraining/{id}")
    public TrainingEntity getTrainingById(@PathVariable Long id) {
        return trainingService.findTrainingById(id);
    }

    @GetMapping("/getTrainingPeriod/{period}")
    public TrainingHolder getTrainingByPeriod(@PathVariable String period) {
        TrainingHolder holder = new TrainingHolder();
        List<DayHolder> days = holder.getDays();
        days.add(new DayHolder(trainingService.findTrainingByPeriodDay(period, "lundi"), "lundi"));
        days.add(new DayHolder(trainingService.findTrainingByPeriodDay(period, "mardi"), "mardi"));
        days.add(new DayHolder(trainingService.findTrainingByPeriodDay(period, "mercredi"), "mercredi"));
        days.add(new DayHolder(trainingService.findTrainingByPeriodDay(period, "jeudi"), "jeudi"));
        days.add(new DayHolder(trainingService.findTrainingByPeriodDay(period, "vendredi"), "vendredi"));
        days.add(new DayHolder(trainingService.findTrainingByPeriodDay(period, "samedi"), "samedi"));
        days.add(new DayHolder(trainingService.findTrainingByPeriodDay(period, "dimanche"), "dimanche"));
        holder.setDays(days);
        return holder;
    }

    @PutMapping("/gestion/updateTraining/{id}")
    public TrainingEntity updateTraining(@PathVariable final long id, @RequestBody final JsonNode values) {
        final TrainingEntity toUpdate = trainingService.findTrainingById(id);
        setTrainingFromJson(values, toUpdate);
        return trainingService.updateTraining(toUpdate);
    }

    @PostMapping("/gestion/addTraining")
    public TrainingEntity addTraining(@RequestBody final JsonNode values) {
        TrainingEntity toAdd = new TrainingEntity();
        setTrainingFromJson(values, toAdd);
        return trainingService.saveTraining(toAdd);
    }

    @DeleteMapping("/gestion/deleteTraining/{id}")
    public ResponseEntity<ResponseStatus> deleteTraining(@PathVariable Long id) {
        return super.getDeleteResponseStatus(id);
    }

    @Override
    protected TrainingEntity getEntity(long id) {
        return trainingService.findTrainingById(id);
    }

    @Override
    protected String deleteEntity(TrainingEntity entity) {
        return trainingService.deleteTraining((long) entity.getId_training());
    }

    private void setTrainingFromJson(final JsonNode values, final TrainingEntity teamToUpdate) {
        final JsonNode dayNode = values.get("day_training"),
                cloakroomNode = values.get("cloakroom_training"),
                startHourNode = values.get("start_hour_training"),
                endHourNode = values.get("end_hour_training"),
                periodNode = values.get("period"),
                teamsNode = values.get("teams");

        if (dayNode != null) {
            teamToUpdate.setDay_training(dayNode.textValue());
        }
        if (cloakroomNode != null) {
            teamToUpdate.setCloakroom_training(cloakroomNode.textValue());
        }
        if (startHourNode != null) {
            teamToUpdate.setStart_hour_training(startHourNode.asInt());
        }
        if (endHourNode != null) {
            teamToUpdate.setEnd_hour_training(endHourNode.asInt());
        }
        if (periodNode != null) {
            teamToUpdate.setPeriod(periodNode.textValue());
        }
        if (teamsNode != null) {
            final Collection<TeamsEntity> nextTeams = new ArrayList<>();
            for (final JsonNode teamNode : teamsNode) {
                final long teamId = teamNode.asLong();
                final TeamsEntity teamsEntity = teamService.findTeamById(teamId);
                if (teamsEntity != null) {
                    nextTeams.add(teamsEntity);
                }
            }
            teamToUpdate.setTeamsEntities(nextTeams);
        }
    }

}
