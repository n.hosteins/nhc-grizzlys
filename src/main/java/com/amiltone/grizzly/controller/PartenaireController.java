package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.NewsEntity;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.entity.PartenaireEntity;
import com.amiltone.grizzly.service.PartenaireService;
import com.amiltone.grizzly.service.PictureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.mail.iap.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin("*")
@RestController
public class PartenaireController extends EntityController<PartenaireEntity>{
    @Autowired
    private PartenaireService partenaireService;
    @Autowired
    private PictureService pictureService;


    @GetMapping("/getAllPartners")
    public List<PartenaireEntity> getAllPartenaires () { return partenaireService.getAllPartenaires();}

    @GetMapping("/getOnePartner/{id}")
    public PartenaireEntity findPartenairesyId (@PathVariable Long id) { return partenaireService.findPartenairesById(id);}

    @PostMapping("/gestion/addPartner")
    public ResponseEntity addPartenaire (@RequestParam("partenaire") String partenaire, @RequestPart(value = "files",required = false) MultipartFile files) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        PartenaireEntity p = mapper.readValue(partenaire, PartenaireEntity.class);

        p.setPriority(partenaireService.count());

        ResponseEntity response = null;
        try {
            pictureService.checkPictureFile(p.getPicture());
            pictureService.replacePictureFile(p.getPicture(), files);
        }
        catch (IOException e){
            response = new ResponseEntity(new ResponseStatus("warning ", e.getMessage()), HttpStatus.CONFLICT);
        }

        if(response == null){
            p = partenaireService.addPartenaire(p);
            if(p == null){
                response = new ResponseEntity(new ResponseStatus("error","Le partenaire n'a pas pu être créé"), HttpStatus.CONFLICT);
            }else{
                response = new ResponseEntity(new ResponseStatus("success","Le partenaire a bien été créé"), HttpStatus.OK);
            }

        }
        return response;
    }

    @PutMapping("/gestion/updatePartner/{id}")
    public ResponseEntity updatePartenaire (@PathVariable Long id, @RequestParam("partenaire") String partenaire, @RequestParam(value="files", required = false) MultipartFile files)throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        PartenaireEntity p = mapper.readValue(partenaire, PartenaireEntity.class);

        pictureService.checkPictureFile(p.getPicture());
        pictureService.replacePictureFile(p.getPicture(), files);

        partenaireService.updatePartenaire(id, p);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/gestion/deletePartner/{id}")
    public ResponseEntity<ResponseStatus> deletePartner (@PathVariable Long id) {
        return super.getDeleteResponseStatus(id);
    }

    @Override
    protected PartenaireEntity getEntity(long id) {
        return partenaireService.findPartenairesById(id);
    }

    @Override
    protected String deleteEntity(PartenaireEntity entity) {
        pictureService.deletePictureFile(entity.getPicture());
        return partenaireService.deletePartenaire((long) entity.getId_partner());
    }
}
