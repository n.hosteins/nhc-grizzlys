package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.storage.FilesStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("*")
public class PictureController {

        @Autowired
        FilesStorageService storageService;

        @GetMapping("/files/{path}/{filename:.+}")
        public ResponseEntity<Resource> getFile(@PathVariable String filename, @PathVariable String path) {
            Resource file = storageService.load(path+"/"+filename);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
        }
    }


