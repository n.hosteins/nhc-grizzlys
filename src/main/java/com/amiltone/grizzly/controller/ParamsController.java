package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.ParamsEntity;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.EmailService;
import com.amiltone.grizzly.service.ParamsService;
import com.amiltone.grizzly.service.PictureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;

@CrossOrigin("*")
@RestController
public class ParamsController {

    @Autowired
    private EmailService emailService;
    @Autowired
    private ParamsService paramsService;
    @Autowired
    private PictureService pictureService;

    @GetMapping("/getParams")
    public ParamsEntity getInfo(){
        return paramsService.findParamsById(1L);
    }

    @PostMapping("/sendMail")
    public void sendMail (@RequestParam("params") String params) throws FileNotFoundException {
        emailService.sendSimpleMessage(params);
    }

    @PutMapping("/gestion/updateParams")
    public ResponseEntity updateContact (@RequestParam("params") String params, @RequestParam(value="files", required = false) MultipartFile files) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ParamsEntity p = mapper.readValue(params, ParamsEntity.class);

        ResponseEntity response = null;
        try {
            pictureService.checkPictureFile(p.getPicture());
            pictureService.replacePictureFile(p.getPicture(), files);
        }
        catch(IOException e){

            response = new ResponseEntity(new com.amiltone.grizzly.response.ResponseStatus("warning ", "Le nom de l'image " + p.getPicture().getName_picture().split("/")[1] + " est déjà utilisé.").toString(), HttpStatus.CONFLICT);
        }

        if(response == null){
            paramsService.updateParams(1L, p);
            if(p == null){
                response = new ResponseEntity(new com.amiltone.grizzly.response.ResponseStatus("error","Les paramêtres n'ont pas pu être mis à jour").toString(), HttpStatus.CONFLICT);
            }else{
                response = new ResponseEntity(new ResponseStatus("success","Les paramêtres ont bien été mis à jour").toString(), HttpStatus.OK);
            }

        }
        return response;
    }
}
