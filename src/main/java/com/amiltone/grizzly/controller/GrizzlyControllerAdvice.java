package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.GrizzlyException;
import com.amiltone.grizzly.response.ResponseStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GrizzlyControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<ResponseStatus> handleError(final Exception error) {
        final String message;
        if (error instanceof GrizzlyException) {
            message = error.getLocalizedMessage();
        } else {
            message = "Erreur inconnue : " + error.getLocalizedMessage();
        }
        final ResponseStatus response = new ResponseStatus("error", message);
        return ResponseEntity.status(500).body(response);
    }
}
