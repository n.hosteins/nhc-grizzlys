package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.UserEntity;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@CrossOrigin("*")
@Controller
/**
 * Le controlleur va gérer les chemins vers les différentes functionnalités
 */
public class AuthController {

    /**
     * autoconstruction de UserService
     */

    @Autowired
    UserService service;

    /**
     * Le end point pourra être remplacé par la page basique du site internet
     * @return hello world (à changer)
     */

    @GetMapping("/")
    public String indexVisiteur(){
        return "/visiteur/index.html";
    }
    @GetMapping("/gestion")
    public String indexAdmin(){
        return "/gestion/index.html";
    }
    @GetMapping("/gestion/{?:actualites|equipes|partenaires|entrainements|association|matchs|parametres}")
    public String indexAll(){
        return "/gestion";
    }
    @GetMapping("/gestion/{?:actualites|equipes|partenaires|entrainements|association|match|parametres}/edition/{?:[0-9]+}")
    public String indexAllEdit(){
        return "/gestion";
    }
    @GetMapping("/gestion/{?:actualites|equipes|partenaires|entrainements|association|match|parametres}/{?:add[A-z]+}")
    public String indexAllAdd(){
        return "/gestion";
    }

    @GetMapping("/connexion")
    public String indexConnexion(){
        return "/gestion";
    }

    /**
     * Cette fonction permet d'ajouter un utilisateur dans la BDD en passant par l'API
     * @param user fait référence à l'utilisateur entré dans l'URL
     * @return renvoie les infos de l'utilisateur vers la BDD
     */
    @PostMapping("/gestion/addUser")
    public ResponseEntity<ResponseStatus> addingUser(UserEntity user){
        System.out.println(user.getUsername());
        final String message = service.addPerson(user);
        return ResponseEntity.ok(ResponseStatus.success(message));
    }

}
