package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.MembreAssoEntity;
import com.amiltone.grizzly.entity.NewsEntity;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.MembreAssoService;
import com.amiltone.grizzly.service.PictureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin("*")
@RestController
public class MembreAssoController extends EntityController<MembreAssoEntity> {

    @Autowired
    private MembreAssoService membreAssoService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/getAllMembreAsso")
    public List<MembreAssoEntity> getAllMembreAsso() {
        return membreAssoService.getAllMembreAsso();
    }

    @GetMapping("/getOneMembreAsso/{id}")
    public MembreAssoEntity getOneMembreAsso(@PathVariable Long id) {
        return membreAssoService.getOneMembreAsso(id);
    }

    @PostMapping("/gestion/addMembreAsso")
    public ResponseEntity addMembreAsso(@RequestParam("membreAsso") String membreAsso, @RequestParam(value = "files", required = false) MultipartFile files) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        MembreAssoEntity m = mapper.readValue(membreAsso, MembreAssoEntity.class);

        m.setPriority(membreAssoService.count());

        ResponseEntity response = null;
        try {
            pictureService.checkPictureFile(m.getPicture());
            pictureService.replacePictureFile(m.getPicture(), files);
        } catch (IOException e) {

            response = new ResponseEntity(new ResponseStatus("warning ", "Le nom de l'image " + m.getPicture().getName_picture().split("/")[1] + " est déjà utilisé.").toString(), HttpStatus.CONFLICT);
        }

        if (response == null) {
            m = membreAssoService.addMembreAsso(m);
            if (m == null) {
                response = new ResponseEntity(new ResponseStatus("error", "Le membre de l'association n'a pas pu être créé").toString(), HttpStatus.CONFLICT);
            } else {
                response = new ResponseEntity(new ResponseStatus("success", "Le membre de l'association a bien été créé").toString(), HttpStatus.OK);
            }

        }
        return response;
    }

    @PutMapping("/gestion/updateMembreAsso/{id}")
    public ResponseEntity<ResponseStatus> updateMembreAsso(@PathVariable Long id, @RequestParam("membreAsso") String membreAsso, @RequestParam(value = "files", required = false) MultipartFile files) throws IOException {
        MembreAssoEntity membre = objectMapper.readValue(membreAsso, MembreAssoEntity.class);
        pictureService.checkPictureFile(membre.getPicture());
        pictureService.replacePictureFile(membre.getPicture(), files);
        membre = membreAssoService.updateMembreAsso(id, membre);
        if (membre == null) {
            return ResponseEntity.status(500).body(new ResponseStatus("error", "Le membre de l'association n'a pas pu être mis à jour"));
        }
        return ResponseEntity.ok(new ResponseStatus("success", "Le membre de l'association a bien été mise à jour"));
    }

    @DeleteMapping("/gestion/deleteMembreAsso/{id}")
    public ResponseEntity deleteMembreAssoById(@PathVariable Long id) throws IOException {
        return super.getDeleteResponseStatus(id);
    }

    @Override
    protected MembreAssoEntity getEntity(long id) {
        return membreAssoService.getOneMembreAsso(id);
    }

    @Override
    protected String deleteEntity(MembreAssoEntity entity) {
        pictureService.deletePictureFile(entity.getPicture());
        return membreAssoService.deleteMembreAsso((long) entity.getId_membre_asso());
    }
}
