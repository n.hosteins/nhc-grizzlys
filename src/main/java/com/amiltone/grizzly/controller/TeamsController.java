package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.TeamsEntity;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@CrossOrigin("*")
@RestController
public class TeamsController extends EntityController<TeamsEntity>{

    @Autowired
    private TeamsService teamsService;
    @Autowired
    private TrainingService trainingService ;
    @Autowired
    private NewsService newsService ;
    @Autowired
    private MatchService matchService ;
    @Autowired
    private PictureService pictureService;



    @GetMapping("/getAllTeams")
    public List<TeamsEntity> getAllTeams () { return teamsService.findAllTeams();}

    @GetMapping("/getTeam/{id}")
    public TeamsEntity findTeamById (@PathVariable Long id) { return teamsService.findTeamById(id);}

    @PostMapping("/gestion/addTeam")
    public ResponseEntity addTeam (
            @RequestParam("team") String team,
            @RequestPart(value = "files", required = false) MultipartFile files,
            @RequestPart(value="fileSup1", required = false) MultipartFile fileSup1,
            @RequestPart(value="fileSup2", required = false) MultipartFile fileSup2,
            @RequestPart(value="fileSup3", required = false) MultipartFile fileSup3
    ) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        TeamsEntity t = mapper.readValue(team, TeamsEntity.class);

        pictureService.checkPictureFile(t.getPicture());
        pictureService.checkPictureFile(t.getPic_supervisor1());
        pictureService.checkPictureFile(t.getPic_supervisor2());
        pictureService.checkPictureFile(t.getPic_supervisor3());

        pictureService.replacePictureFile(t.getPicture(), files);
        pictureService.replacePictureFile(t.getPic_supervisor1(), fileSup1);
        pictureService.replacePictureFile(t.getPic_supervisor2(), fileSup2);
        pictureService.replacePictureFile(t.getPic_supervisor3(), fileSup3);

        t.setPriority(teamsService.count());

        teamsService.addTeam(t);
        return ResponseEntity.ok().build();
    }

    @Autowired
    private ObjectMapper objectMapper;

    @PutMapping("/gestion/updateTeam/{id}")
    public ResponseEntity updateTeam (
            @PathVariable Long id,
            @RequestParam("team") String team,
            @RequestPart(value="files", required = false) MultipartFile files,
            @RequestPart(value="fileSup1", required = false) MultipartFile fileSup1,
            @RequestPart(value="fileSup2", required = false) MultipartFile fileSup2,
            @RequestPart(value="fileSup3", required = false) MultipartFile fileSup3
    ) throws IOException {

        TeamsEntity t = objectMapper.readValue(team, TeamsEntity.class);

        pictureService.checkPictureFile(t.getPicture());
        pictureService.checkPictureFile(t.getPic_supervisor1());
        pictureService.checkPictureFile(t.getPic_supervisor2());
        pictureService.checkPictureFile(t.getPic_supervisor3());

        pictureService.replacePictureFile(t.getPicture(), files);
        pictureService.replacePictureFile(t.getPic_supervisor1(), fileSup1);
        pictureService.replacePictureFile(t.getPic_supervisor2(), fileSup2);
        pictureService.replacePictureFile(t.getPic_supervisor3(), fileSup3);

        ArrayList<Integer> idPicTab = new ArrayList<>();

        teamsService.updateTeam(id, t);

        for(int idPic: idPicTab){
            System.out.println(pictureService.deletePictureById(idPic));
        }

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/gestion/deleteTeam/{id}")
    public ResponseEntity<ResponseStatus> deletePartner (@PathVariable Long id) {
        return super.getDeleteResponseStatus(id);
    }

    @Override
    protected TeamsEntity getEntity(long id) {
        return teamsService.findTeamById(id);
    }

    @Override
    protected String deleteEntity(TeamsEntity entity) {
        long id = (long) entity.getId_team_teams();
        pictureService.deletePictureFile(entity.getPicture());
        pictureService.deletePictureFile(entity.getPic_supervisor1());
        pictureService.deletePictureFile(entity.getPic_supervisor2());
        pictureService.deletePictureFile(entity.getPic_supervisor3());
        matchService.removeMatchTeam(id);
        newsService.removeTeam(id);
        trainingService.removeTeam(id);
        return teamsService.deleteTeamById(id);
    }
}