package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.MatchEntity;
import com.amiltone.grizzly.holder.MatchHolder;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.MatchService;
import com.amiltone.grizzly.service.TeamsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@CrossOrigin("*")
@RestController
public class MatchController extends EntityController<MatchEntity> {
    @Autowired
    MatchService matchService;
    @Autowired
    private TeamsService teamService;

    @GetMapping("/get20Match/{id}")
    public MatchHolder find20MatchByDate (@PathVariable Long id) {
        return matchService.find20Match(id);
    }

    @GetMapping("/getAllMatch")
    public List<MatchEntity> findAllMatchByDate () {
        return matchService.findAllMatchByDate();
    }

    @GetMapping("/getMatchTeam/{id}")
    public List<MatchEntity> findMatchByIdTeam (@PathVariable Long id) {
        return matchService.findMatchByIdTeam(id);
    }

    @GetMapping("/getMatchDate/{date}")
    public List<MatchEntity> findMatchByDate(@PathVariable String date) {
        return matchService.findMatchByDate(date);
    }

    @PostMapping("/gestion/addMatch")
    public ResponseEntity addMatch(@RequestParam("match") String match) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        MatchEntity m = mapper.readValue(match, MatchEntity.class);

        ResponseEntity response = null;

        m.setTeam(teamService.findTeamById(m.getTeam().getId_team_teams()));
        m = matchService.saveMatch(m);
        if(m == null){
            response = new ResponseEntity(new ResponseStatus("error","Le match n'a pas pu être créé").toString(), HttpStatus.CONFLICT);
        }else{

            response = new ResponseEntity(new ResponseStatus("success", "Le match a bien été créé").toString(), HttpStatus.OK);
        }
        return response;
    }


    @PutMapping("/gestion/updateMatch/{id}")
    public ResponseEntity updateMatch(@PathVariable Long id, @RequestParam("match") String match) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        MatchEntity m = mapper.readValue(match, MatchEntity.class);

        ResponseEntity response = null;

        m.setTeam(teamService.findTeamById(m.getTeam().getId_team_teams()));
        matchService.updateMatch(id, m);
        if (m == null) {
            response = new ResponseEntity(new com.amiltone.grizzly.response.ResponseStatus("error", "Le match n'a pas pu être mis à jour").toString(), HttpStatus.CONFLICT);
        } else {

            response = new ResponseEntity(new ResponseStatus("success", "Le match a bien été mis à jour").toString(), HttpStatus.OK);
        }
        return response;
    }

    @DeleteMapping("/gestion/deleteMatch/{id}")
    public ResponseEntity deleteMatch(@PathVariable Long id) {
        return super.getDeleteResponseStatus(id);
    }


    @Override
    protected MatchEntity getEntity(long id) {
        return matchService.findMatchById(id);
    }

    @Override
    protected String deleteEntity(MatchEntity entity) {
        return matchService.deleteMatch(entity.getId_match());
    }
}
