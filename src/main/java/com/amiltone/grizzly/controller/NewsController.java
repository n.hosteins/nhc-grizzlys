package com.amiltone.grizzly.controller;

import com.amiltone.grizzly.entity.MatchEntity;
import com.amiltone.grizzly.entity.NewsEntity;
import com.amiltone.grizzly.holder.NewsHolder;
import com.amiltone.grizzly.entity.TeamsEntity;
import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.NewsService;
import com.amiltone.grizzly.service.PictureService;
import com.amiltone.grizzly.service.TeamsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@CrossOrigin("*")
@RestController
public class NewsController extends EntityController<NewsEntity> {
    @Autowired
    NewsService newsService;
    @Autowired
    private TeamsService teamService ;
    @Autowired
    private PictureService pictureService;

    @GetMapping("/getNewsById/{id}")
    public NewsEntity findNewsById (@PathVariable Long id) {
        return newsService.findNewsById(id);
    }

    @GetMapping("/getNews/{index}")
    public NewsHolder find3NewsByDate (@PathVariable int index) {
        return newsService.find3NewsByDate(index);
    }

    @GetMapping("/getAllNews")
    public List<NewsEntity> findAllNewsByDate () {
        return newsService.findAllNewsByDate();
    }

    @GetMapping("/get5News/{index}")
    public NewsHolder find5NewsByDate (@PathVariable int index) {
        return newsService.find5NewsByDate(index);
    }

    @GetMapping("/get20News/{index}")
    public NewsHolder find20NewsByDate (@PathVariable int index) {
        return newsService.find20NewsByDate(index);
    }

    @GetMapping("/getNewsByTitle/{title}")
    public List<NewsEntity> findNewsByTitle (@PathVariable String title) {
        return newsService.findNewsByTitle(title);
    }

    @PostMapping("/gestion/addNews")
    public ResponseEntity<ResponseStatus> addNews (@RequestParam("news") String news, @RequestParam(value = "tags", required = false) String tags, @RequestParam(value = "files", required = false) MultipartFile files) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final NewsEntity newsEntity = mapper.readValue(news, NewsEntity.class);
        if(tags != null && !tags.isEmpty()) {
            setNewsFromJson(new JSONObject(tags), newsEntity);
        }
        pictureService.checkPictureFile(newsEntity.getPicture());
        pictureService.replacePictureFile(newsEntity.getPicture(), files);
        newsService.saveNews(newsEntity);
        return ResponseEntity.ok(ResponseStatus.success("Actualité ajoutée"));
    }

    @PutMapping("/gestion/updateNews/{id}")
    public ResponseEntity updateNews (@PathVariable Long id, @RequestParam("news") String news, @RequestParam(value = "tags", required = false) String tags, @RequestPart(value = "files", required = false) MultipartFile files) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        NewsEntity n = mapper.readValue(news, NewsEntity.class);

        if(tags != null && !tags.isEmpty()) {
            setNewsFromJson(new JSONObject(tags), n);
        }
        ResponseEntity response = null;
        try {
            pictureService.checkPictureFile(n.getPicture());
            pictureService.replacePictureFile(n.getPicture(), files);
        }
        catch(IOException e){

            response = new ResponseEntity(new ResponseStatus("warning ", "Le nom de l'image " + n.getPicture().getName_picture().split("/")[1] + " est déjà utilisé.").toString(), HttpStatus.CONFLICT);
        }

        if(response == null){
            n = newsService.updateNews(id, n);
            if(n == null){
                response = new ResponseEntity(new ResponseStatus("error","L'actualité n'a pas pu être mise à jour").toString(), HttpStatus.CONFLICT);
            }else{
                response = new ResponseEntity(new ResponseStatus("success","L'actualité a bien été mise à jour").toString(), HttpStatus.OK);
            }

        }
        return response;
    }

    @DeleteMapping("/gestion/deleteNews/{id}")
    public ResponseEntity<ResponseStatus> deleteNews(@PathVariable final long id) {
        return super.getDeleteResponseStatus(id);
    }

    private void setNewsFromJson(JSONObject obj, NewsEntity newsEntity) {


        JSONArray arr = obj.getJSONArray("teams");
        Collection<TeamsEntity> news_teams = new ArrayList<>();
        for (int i = 0; i < arr.length(); i++)
        {
            Long teams = arr.getLong(i);
            TeamsEntity team = teamService.findTeamById(teams);
            news_teams.add(team);
        }
        newsEntity.setTeams(news_teams);
    }

    @Override
    protected NewsEntity getEntity(long id) {
        return newsService.findNewsById(id);
    }

    @Override
    protected String deleteEntity(NewsEntity entity) {
        pictureService.deletePictureFile(entity.getPicture());
        return newsService.deleteNews((long) entity.getId_news());
    }
}
