package com.amiltone.grizzly.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FilesStorageService {
    public void save(MultipartFile file,String name_picture);

    public Resource load(String filename);

    public void deletePicture(String filename) throws IOException;

    public void updatePicture(String old_name, String new_name) throws IOException;
}
