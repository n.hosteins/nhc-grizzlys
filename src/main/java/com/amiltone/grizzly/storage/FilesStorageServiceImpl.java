package com.amiltone.grizzly.storage;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import com.amiltone.grizzly.GrizzlyException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {

    private final Path root = Paths.get("uploads");

    @Override
    public void save(MultipartFile file, String name_picture) {
        try {
            Files.copy(file.getInputStream(), Paths.get(root +"/"+ name_picture),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new GrizzlyException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void deletePicture(String name_picture) throws IOException {
        Path fileToDeletePath = Paths.get(root+"/"+name_picture);
        final File fromFile = fileToDeletePath.toFile();
        if (!fromFile.exists()) {
            return;
        }
        Files.delete(fileToDeletePath);
    }

    public void updatePicture(final String oldName, final String newName) throws IOException {
        if (StringUtils.isEmpty(oldName)) {
            return;
        }
        final Path fromPath = root.resolve(oldName),
                toPath = root.resolve(newName);
        final File fromFile = fromPath.toFile();
        if (!fromFile.exists()) {
            return;
        }
        Files.move(fromPath, toPath);
    }

}
