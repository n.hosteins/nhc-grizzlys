package com.amiltone.grizzly.entity;

import org.springframework.lang.Nullable;

import java.util.Objects;

public class EntityUtils {
    public static PictureEntity getPicture(@Nullable final PictureEntity picture) {
        return Objects.requireNonNullElse(picture, PictureEntity.DEFAULT);
    }

    private EntityUtils() {}
}
