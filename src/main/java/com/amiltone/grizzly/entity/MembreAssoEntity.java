package com.amiltone.grizzly.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "membre_asso")
public class MembreAssoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_membre_asso;
    @Column(columnDefinition="VARCHAR(30)")
    private String firstname_membre_asso ;
    @Column(columnDefinition="VARCHAR(30)")
    private String lastname_membre_asso;
    @Column(columnDefinition="VARCHAR(10)")
    private String phone_membre_asso;
    @Column(columnDefinition="VARCHAR(50)")
    private String mail_membre_asso;
    private String description_membre_asso;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity picture;
    private Long priority;

    public Long getId_membre_asso() {
        return id_membre_asso;
    }

    public String getFirstname_membre_asso() {
        return firstname_membre_asso;
    }

    public void setFirstname_membre_asso(String firstname_membre_asso) {
        this.firstname_membre_asso = firstname_membre_asso;
    }

    public String getLastname_membre_asso() {
        return lastname_membre_asso;
    }

    public void setLastname_membre_asso(String lastname_membre_asso) {
        this.lastname_membre_asso = lastname_membre_asso;
    }

    public String getPhone_membre_asso() {
        return phone_membre_asso;
    }

    public void setPhone_membre_asso(String phone_membre_asso) {
        this.phone_membre_asso = phone_membre_asso;
    }

    public String getMail_membre_asso() {
        return mail_membre_asso;
    }

    public void setMail_membre_asso(String mail_membre_asso) {
        this.mail_membre_asso = mail_membre_asso;
    }

    public String getDescription_membre_asso() {
        return description_membre_asso;
    }

    public void setDescription_membre_asso(String description_membre_asso) {
        this.description_membre_asso = description_membre_asso;
    }

    public PictureEntity getPicture() {
        return EntityUtils.getPicture(picture);
    }

    public void setPicture(PictureEntity picture) {
        this.picture = picture;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }
}
