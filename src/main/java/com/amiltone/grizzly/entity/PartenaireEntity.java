package com.amiltone.grizzly.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "partenaires")
public class PartenaireEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_partner;
    @Column(columnDefinition="VARCHAR(50)")
    private String name_partner ;
    private String description_partner;
    @Column(columnDefinition="VARCHAR(40)")
    private String city_partner ;
    @Column(columnDefinition="VARCHAR(5)")
    private String zip_partner ;
    private String street_partner ;
    @Column(columnDefinition="VARCHAR(10)")
    private String phone_partner;
    @Column(columnDefinition="VARCHAR(50)")
    private String mail_partner;
    @Column(columnDefinition="VARCHAR(100)")
    private String website_partner;
    private Boolean institutional;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity picture;
    private Long priority;

    public Long getId_partner() {
        return id_partner;
    }

    public String getName_partner() {
        return name_partner;
    }

    public void setName_partner(String name_partner) {
        this.name_partner = name_partner;
    }

    public String getDescription_partner() {
        return description_partner;
    }

    public void setDescription_partner(String description_partner) {
        this.description_partner = description_partner;
    }

    public String getCity_partner() {
        return city_partner;
    }

    public void setCity_partner(String city_partner) {
        this.city_partner = city_partner;
    }

    public String getStreet_partner() {
        return street_partner;
    }

    public void setStreet_partner(String street_partner) {
        this.street_partner = street_partner;
    }

    public String getMail_partner() {
        return mail_partner;
    }

    public void setMail_partner(String mail_partner) {
        this.mail_partner = mail_partner;
    }

    public String getWebsite_partner() {
        return website_partner;
    }

    public void setWebsite_partner(String website_partner) {
        this.website_partner = website_partner;
    }

    public PictureEntity getPicture() {
        return EntityUtils.getPicture(picture);
    }

    public void setPicture(PictureEntity picture) {
        this.picture = picture;
    }

    public String getZip_partner() {
        return zip_partner;
    }

    public void setZip_partner(String zip_partner) {
        this.zip_partner = zip_partner;
    }

    public String getPhone_partner() {
        return phone_partner;
    }

    public void setPhone_partner(String phone_partner) {
        this.phone_partner = phone_partner;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Boolean getInstitutional() {
        return institutional;
    }

    public void setInstitutional(Boolean institutional) {
        this.institutional = institutional;
    }
}
