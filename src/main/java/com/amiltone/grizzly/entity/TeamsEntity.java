package com.amiltone.grizzly.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "teams")
public class TeamsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long  id_team_teams ;
    @Column(columnDefinition="VARCHAR(20)")
    private String name_team;
    @Column(columnDefinition="TEXT")
    private String presentation_team ;
    @JsonIgnoreProperties({"teams", "trainings"})
    @ManyToMany(mappedBy="teams", fetch = FetchType.LAZY)
    Collection<TrainingEntity> trainings = new ArrayList<TrainingEntity>();
    @JsonIgnoreProperties({"teams", "news"})
    @ManyToMany(mappedBy="teams", fetch = FetchType.LAZY)
    Collection<NewsEntity> news = new ArrayList<NewsEntity>();
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity picture;

    @OneToMany(cascade=CascadeType.ALL)
    private List<SupervisorEntity> supervisors;

    @Column(columnDefinition="VARCHAR(50)")
    private String supervisor_1;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pic_supervisor1", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity pic_supervisor1;

    @Column(columnDefinition="VARCHAR(50)")
    private String function_supervisor1;
    @Column(columnDefinition="VARCHAR(50)")
    private String mail_supervisor1;

    @Column(columnDefinition="VARCHAR(50)")
    private String supervisor_2;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pic_supervisor2", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity pic_supervisor2;

    @Column(columnDefinition="VARCHAR(50)")
    private String function_supervisor2;
    @Column(columnDefinition="VARCHAR(50)")
    private String mail_supervisor2;

    @Column(columnDefinition="VARCHAR(50)")
    private String supervisor_3;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pic_supervisor3", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity pic_supervisor3;

    @Column(columnDefinition="VARCHAR(50)")
    private String function_supervisor3;
    @Column(columnDefinition="VARCHAR(50)")
    private String mail_supervisor3;
    private Long priority;

    public Long getId_team_teams() {
        return id_team_teams;
    }

    public String getName_team() {
        return name_team;
    }

    public void setName_team(String name_team) {
        this.name_team = name_team;
    }

    public String getPresentation_team() {
        return presentation_team;
    }

    public void setPresentation_team(String presentation_team) {
        this.presentation_team = presentation_team;
    }

    public Collection<TrainingEntity> getTrainings() {
        return trainings;
    }

    public void setTrainings(Collection<TrainingEntity> trainings) {
        this.trainings = trainings;
    }

    public Collection<NewsEntity> getNews() {
        return news;
    }

    public void setNews(Collection<NewsEntity> news) {
        this.news = news;
    }

    public PictureEntity getPicture() {
        return picture;
    }

    public void setPicture(PictureEntity picture) {
        this.picture = picture;
    }

    public String getSupervisor_1() {
        return supervisor_1;
    }

    public void setSupervisor_1(String supervisor_1) {
        this.supervisor_1 = supervisor_1;
    }

    public PictureEntity getPic_supervisor1() {
        return EntityUtils.getPicture(pic_supervisor1);
    }

    public void setPic_supervisor1(PictureEntity pic_supervisor1) {
        this.pic_supervisor1 = pic_supervisor1;
    }

    public String getFunction_supervisor1() {
        return function_supervisor1;
    }

    public void setFunction_supervisor1(String function_supervisor1) {
        this.function_supervisor1 = function_supervisor1;
    }

    public String getSupervisor_2() {
        return supervisor_2;
    }

    public void setSupervisor_2(String supervisor_2) {
        this.supervisor_2 = supervisor_2;
    }

    public PictureEntity getPic_supervisor2() {

        return EntityUtils.getPicture(pic_supervisor2);

    }

    public void setPic_supervisor2(PictureEntity pic_supervisor2) {
        this.pic_supervisor2 = pic_supervisor2;
    }

    public String getFunction_supervisor2() {
        return function_supervisor2;
    }

    public void setFunction_supervisor2(String function_supervisor2) {
        this.function_supervisor2 = function_supervisor2;
    }

    public String getSupervisor_3() {
        return supervisor_3;
    }

    public void setSupervisor_3(String supervisor_3) {
        this.supervisor_3 = supervisor_3;
    }

    public PictureEntity getPic_supervisor3() {


        return EntityUtils.getPicture(pic_supervisor3);
    }

    public void setPic_supervisor3(PictureEntity pic_supervisor3) {
        this.pic_supervisor3 = pic_supervisor3;
    }

    public String getFunction_supervisor3() {
        return function_supervisor3;
    }

    public void setFunction_supervisor3(String function_supervisor3) {
        this.function_supervisor3 = function_supervisor3;
    }

    public String getMail_supervisor1() {
        return mail_supervisor1;
    }

    public void setMail_supervisor1(String mail_supervisor1) {
        this.mail_supervisor1 = mail_supervisor1;
    }

    public String getMail_supervisor2() {
        return mail_supervisor2;
    }

    public void setMail_supervisor2(String mail_supervisor2) {
        this.mail_supervisor2 = mail_supervisor2;
    }

    public String getMail_supervisor3() {
        return mail_supervisor3;
    }

    public void setMail_supervisor3(String mail_supervisor3) {
        this.mail_supervisor3 = mail_supervisor3;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public List<SupervisorEntity> getSupervisors() {
        return supervisors;
    }

    public void setSupervisors(List<SupervisorEntity> supervisors) {
        this.supervisors = supervisors;
    }

}
