package com.amiltone.grizzly.entity;

import javax.persistence.*;

/**
 * testUser est l'entité utilisateur (dans le cadre du projet : l'admin et d'éventuels éditeurs si besoin est)
 * Il n'y a pas encore de gestion de rôle, mais celà pourrait être gérée par une enum ou une tabe en plus
 */

@Entity
@Table(name = "users")
public class UserEntity {

    /**
     * L'id a été déclaré par l'annotation @Id, disant à spring qu'il s'agit de l'id d'une table
     * Generated value stipule que l'id ne se verra pas octroyé une valeur lors de la création d'une nouvelle entité
     * Lombok gère les getters et les setters (assurés par les annotation ci dessus)
     */


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Id
    @Column(columnDefinition="VARCHAR(30)")
    private String username;

    @Column(columnDefinition="VARCHAR(255)")
    private String password;

    private Boolean enabled;

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
