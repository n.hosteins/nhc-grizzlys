package com.amiltone.grizzly.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "params")
public class ParamsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_params ;
    private String meta_text;
    @Column(columnDefinition="VARCHAR(50)")
    private String meta_title;
    @Column(columnDefinition="VARCHAR(30)")
    private String home_title;
    @Column(columnDefinition="VARCHAR(100)")
    private String home_slogan;
    @Column(columnDefinition="VARCHAR(15)")
    private String home_values_1;
    @Column(columnDefinition="VARCHAR(15)")
    private String home_values_2;
    @Column(columnDefinition="VARCHAR(15)")
    private String home_values_3;
    @Column(columnDefinition="VARCHAR(15)")
    private String home_values_4;
    @Column(columnDefinition="VARCHAR(50)")
    private String email_contact;
    private String address_asso;
    @Column(columnDefinition="VARCHAR(10)")
    private String phone_asso;
    @Column(columnDefinition="VARCHAR(50)")
    private String president_asso;
    @Column(columnDefinition="TEXT")
    private String description_nhc;
    @Column(columnDefinition="TEXT")
    private String legal_notice;
    @Column(columnDefinition="VARCHAR(30)")
    private String period_name;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity picture;

    public Long getId_params() {
        return id_params;
    }

    public String getMeta_text() {
        return meta_text;
    }

    public void setMeta_text(String meta_text) {
        this.meta_text = meta_text;
    }

    public String getMeta_title() {
        return meta_title;
    }

    public void setMeta_title(String meta_title) {
        this.meta_title = meta_title;
    }

    public String getEmail_contact() {
        return email_contact;
    }

    public void setEmail_contact(String email_contact) {
        this.email_contact = email_contact;
    }

    public String getAddress_asso() {
        return address_asso;
    }

    public void setAddress_asso(String address_asso) {
        this.address_asso = address_asso;
    }

    public String getPhone_asso() {
        return phone_asso;
    }

    public void setPhone_asso(String phone_asso) {
        this.phone_asso = phone_asso;
    }

    public String getPresident_asso() {
        return president_asso;
    }

    public void setPresident_asso(String president_asso) {
        this.president_asso = president_asso;
    }

    public String getPeriod_name() {
        return period_name;
    }

    public void setPeriod_name(String period_name) {
        this.period_name = period_name;
    }

    public PictureEntity getPicture() {
        return EntityUtils.getPicture(picture);
    }

    public void setPicture(PictureEntity picture) {
        this.picture = picture;
    }

    public String getHome_title() {
        return home_title;
    }

    public void setHome_title(String home_title) {
        this.home_title = home_title;
    }

    public String getHome_slogan() {
        return home_slogan;
    }

    public void setHome_slogan(String home_slogan) {
        this.home_slogan = home_slogan;
    }

    public String getHome_values_1() {
        return home_values_1;
    }

    public void setHome_values_1(String home_values_1) {
        this.home_values_1 = home_values_1;
    }

    public String getHome_values_2() {
        return home_values_2;
    }

    public void setHome_values_2(String home_values_2) {
        this.home_values_2 = home_values_2;
    }

    public String getHome_values_3() {
        return home_values_3;
    }

    public void setHome_values_3(String home_values_3) {
        this.home_values_3 = home_values_3;
    }

    public String getHome_values_4() {
        return home_values_4;
    }

    public void setHome_values_4(String home_values_4) {
        this.home_values_4 = home_values_4;
    }

    public String getDescription_nhc() {
        return description_nhc;
    }

    public void setDescription_nhc(String description_nhc) {
        this.description_nhc = description_nhc;
    }

    public String getLegal_notice() {
        return legal_notice;
    }

    public void setLegal_notice(String legal_notice) {
        this.legal_notice = legal_notice;
    }
}
