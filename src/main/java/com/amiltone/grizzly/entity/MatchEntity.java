package com.amiltone.grizzly.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "event_match")
public class MatchEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long  id_match ;
    @Column(columnDefinition="VARCHAR(10)")
    private String date_match;
    private Integer hour_match;
    @Column(columnDefinition="VARCHAR(40)")
    private String location;
    @Column(columnDefinition="VARCHAR(50)")
    private String title;
    @JsonIgnoreProperties({"news","teams", "trainings", "picture", "pic_supervisor1", "pic_supervisor2", "pic_supervisor3"})
    @ManyToOne
    @JoinColumn(name="team", nullable=false)
    TeamsEntity team;

    public Long getId_match() {
        return id_match;
    }

    public String getDate_match() {
        return date_match;
    }

    public void setDate_match(String date_match) {
        this.date_match = date_match;
    }

    public Integer getHour_match() {
        return hour_match;
    }

    public void setHour_match(Integer hour_match) {
        this.hour_match = hour_match;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TeamsEntity getTeam() {
        return team;
    }

    public void setTeam(TeamsEntity team) {
        this.team = team;
    }
}
