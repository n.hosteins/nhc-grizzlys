package com.amiltone.grizzly.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "trainings")
public class TrainingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_training ;
    @Column(columnDefinition="VARCHAR(10)")
    private String day_training;
    @Column(columnDefinition="VARCHAR(10)")
    private String cloakroom_training;
    private Integer start_hour_training ;
    private Integer end_hour_training ;
    @Column(columnDefinition="VARCHAR(10)")
    private String period;
    @JsonIgnoreProperties({"trainings", "news", "picture", "pic_supervisor1", "pic_supervisor2", "pic_supervisor3"})
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "teams_trainings", referencedColumnName = "id_training")
    Collection<TeamsEntity> teams = new ArrayList<TeamsEntity>();

    public Long getId_training() {
        return id_training;
    }

    public String getDay_training() {
        return day_training;
    }

    public void setDay_training(String day_training) {
        this.day_training = day_training;
    }

    public Integer getStart_hour_training() {
        return start_hour_training;
    }

    public String getCloakroom_training() {
        return cloakroom_training;
    }

    public void setCloakroom_training(String cloakroom_training) {
        this.cloakroom_training = cloakroom_training;
    }

    public void setStart_hour_training(Integer start_hour_training) {
        this.start_hour_training = start_hour_training;
    }

    public Integer getEnd_hour_training() {
        return end_hour_training;
    }

    public void setEnd_hour_training(Integer end_hour_training) {
        this.end_hour_training = end_hour_training;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String school_period) {
        this.period = school_period;
    }

    public Collection<TeamsEntity> getTeams() {
        return teams;
    }

    public void setTeamsEntities(Collection<TeamsEntity> teams) {
        this.teams = teams;
    }
}
