package com.amiltone.grizzly.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@Table(name = "supervisors")
public class SupervisorEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_supervisor;
    private String name_supervisor;
    private String function_supervisor;
    private String mail_supervisor;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pic_supervisor", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity picture;

    public Long getId_supervisor() {
        return id_supervisor;
    }

    public String getName_supervisor() {
        return name_supervisor;
    }

    public void setName_supervisor(String name_supervisor) {
        this.name_supervisor = name_supervisor;
    }

    public String getFunction_supervisor() {
        return function_supervisor;
    }

    public void setFunction_supervisor(String function_supervisor) {
        this.function_supervisor = function_supervisor;
    }

    public String getMail_supervisor() {
        return mail_supervisor;
    }

    public void setMail_supervisor(String mail_supervisor) {
        this.mail_supervisor = mail_supervisor;
    }

    @JsonGetter("picture")
    @NonNull
    public PictureEntity getPicture() {
        return EntityUtils.getPicture(picture);
    }

    public void setPicture(PictureEntity picture) {
        this.picture = picture;
    }
}
