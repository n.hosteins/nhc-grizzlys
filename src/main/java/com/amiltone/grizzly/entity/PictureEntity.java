package com.amiltone.grizzly.entity;

import javax.persistence.*;

@Entity
@Table(name = "pictures")
public class PictureEntity {
    public static PictureEntity DEFAULT = new PictureEntity();

    static {
        DEFAULT.id_picture = 0;
        DEFAULT.setAlt_picture("");
        DEFAULT.setName_picture("");
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_picture")
    private int id_picture;
    private String name_picture;
    private String alt_picture;

    public int getId_picture() {
        return id_picture;
    }

    public String getName_picture() {
        return name_picture;
    }

    public void setName_picture(String name_picture) {
        this.name_picture = name_picture;
    }

    public String getAlt_picture() {
        return alt_picture;
    }

    public void setAlt_picture(String alt_picture) {
        this.alt_picture = alt_picture;
    }


}
