package com.amiltone.grizzly.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "news")
public class NewsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long  id_news ;
    @Column(columnDefinition="VARCHAR(50)")
    private String title_news;
    private Date date_news;
    @Column(columnDefinition="TEXT")
    private String article_news ;
    @JsonIgnoreProperties({"news","teams", "trainings", "picture", "pic_supervisor1", "pic_supervisor2", "pic_supervisor3"})
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "teams_news", referencedColumnName = "id_news")
    Collection<TeamsEntity> teams = new ArrayList<TeamsEntity>();
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "picture_id", referencedColumnName = "id_picture")
    @NotFound(action= NotFoundAction.IGNORE)
    private PictureEntity picture;

    public Long getId_news() {
        return id_news;
    }

    public String getTitle_news() {
        return title_news;
    }

    public void setTitle_news(String title_news) {
        this.title_news = title_news;
    }

    public Date getDate_news() {
        return date_news;
    }

    public void setDate_news(Date date_news) {
        this.date_news = date_news;
    }

    public String getArticle_news() {
        return article_news;
    }

    public void setArticle_news(String article_news) {
        this.article_news = article_news;
    }

    public PictureEntity getPicture() {
        return EntityUtils.getPicture(picture);
    }

    public void setPicture(PictureEntity picture) {
        this.picture = picture;
    }

    public Collection<TeamsEntity> getTeams() {
        return teams;
    }

    public void setTeams(Collection<TeamsEntity> teams) {
        this.teams = teams;
    }
}
