package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.TrainingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TrainingRepository extends JpaRepository<TrainingEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM trainings t WHERE LOWER(t.day_training) = :day AND LOWER(t.period) = :period ORDER BY start_hour_training ASC")
    List<TrainingEntity> findTrainingByPeriodDay(String period, String day);
}
