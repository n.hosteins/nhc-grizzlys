package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.PictureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PictureRepository extends JpaRepository<PictureEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM pictures p WHERE p.name_picture = :name")
    PictureEntity findByName(String name);
}
