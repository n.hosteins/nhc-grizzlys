package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.MembreAssoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MembreAssoRepository extends JpaRepository<MembreAssoEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM membre_asso m ORDER BY m.priority ASC")
    List<MembreAssoEntity> findAllSortByPriority();

}
