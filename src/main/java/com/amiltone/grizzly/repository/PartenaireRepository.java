package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.PartenaireEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PartenaireRepository extends JpaRepository<PartenaireEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM partenaires p ORDER BY p.priority ASC")
    List<PartenaireEntity> findAllSortByPriority();

}
