package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.NewsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NewsRepository extends JpaRepository<NewsEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM news n ORDER BY date_news DESC")
    List<NewsEntity> findAllNewsByDate();

    @Query(nativeQuery = true, value = "SELECT * FROM news n ORDER BY date_news DESC LIMIT :index,3")
    List<NewsEntity> find3NewsByDate(int index);

    @Query(nativeQuery = true, value = "SELECT * FROM news n ORDER BY date_news DESC LIMIT :index,5")
    List<NewsEntity> find5NewsByDate(int index);

    @Query(nativeQuery = true, value = "SELECT * FROM news n ORDER BY date_news DESC LIMIT :index,20")
    List<NewsEntity> find20NewsByDate(int index);

    @Query(nativeQuery = true, value = "SELECT * FROM news n WHERE n.title_news LIKE %:title% ORDER BY date_news DESC LIMIT 20")
    List<NewsEntity> findNewsByTitle(String title);

}