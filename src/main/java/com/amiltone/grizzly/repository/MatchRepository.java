package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.MatchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MatchRepository extends JpaRepository<MatchEntity, Long>{


    @Query(nativeQuery = true, value = "SELECT * FROM event_match m WHERE m.date_match > (SELECT NOW()) ORDER BY date_match ASC, hour_match ASC")
    List<MatchEntity> findAllMatchByDate();

    @Query(nativeQuery = true, value = "SELECT * FROM event_match m ORDER BY date_match ASC LIMIT :index,20")
    List<MatchEntity> find20Match(Long index);

    @Query(nativeQuery = true, value = "SELECT * FROM event_match m WHERE m.team = :id ORDER BY date_match ASC")
    List<MatchEntity> findMatchByIdTeam(Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM event_match m WHERE (Select DATE_FORMAT(m.date_match, \"%Y-%m-%d\")) = :date ")
    List<MatchEntity> findMatchByDate(String date);
}
