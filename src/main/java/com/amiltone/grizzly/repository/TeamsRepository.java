package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.TeamsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeamsRepository extends JpaRepository<TeamsEntity, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM teams t ORDER BY t.priority ASC")
    List<TeamsEntity> findAllSortByPriority();
}
