package com.amiltone.grizzly.repository;

import com.amiltone.grizzly.entity.ParamsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ParamsRepository extends JpaRepository<ParamsEntity, Long> {
}
