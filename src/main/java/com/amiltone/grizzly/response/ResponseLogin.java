package com.amiltone.grizzly.response;

public class ResponseLogin {

    private Boolean is_auth;
    private String username;

    public ResponseLogin(Boolean is_auth, String username) {
        this.is_auth = is_auth;
        this.username = username;
    }

    @Override
    public String toString() {
        return "{\n" +
                "\"is_auth\":" + is_auth +
                ",\n \"username\":\"" + username +
                "\" \n}";
    }
}