package com.amiltone.grizzly.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class ResponseStatus {
    public static ResponseStatus success(final String message) {
        return new ResponseStatus("success", message);
    }

    final private String status;
    final private String message;

    @JsonCreator
    public ResponseStatus(@JsonProperty("status") final String status,
            @JsonProperty("message") final String message) {
        this.status = status;
        this.message = message;
    }

    @JsonGetter("status")
    public String getStatus() {
        return status;
    }

    @JsonGetter("message")
    public String getMessage() {
        return message;
    }
}