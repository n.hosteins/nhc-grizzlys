package com.amiltone.grizzly;

public final class GrizzlyException extends RuntimeException {
    public GrizzlyException(final String message) {
        super(message);
    }
}
