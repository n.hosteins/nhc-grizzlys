package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.ParamsEntity;
import com.amiltone.grizzly.security.AES;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Properties;
import java.util.Scanner;

@Component
public class EmailService {

    private JavaMailSender emailSender;
    @Autowired
    private ParamsService paramsService;

    public void sendSimpleMessage(String params) throws FileNotFoundException {

        emailSender = this.getJavaMailSender();

        ParamsEntity paramsEntity = paramsService.findParamsById(1L);
        JSONObject obj = new JSONObject(params);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(obj.getString("mail_address"));
        message.setTo(paramsEntity.getEmail_contact());
        message.setSubject("Nouveau message depuis le site NHC-Grizzly");
        String text = "Vous avez reçu un message de " + obj.getString("name");
        text +=  " (" + obj.getString("mail_address");
        if(!obj.getString("phone").equals("")){
            text +=  " - " + obj.getString("phone");
        }
        text +=  ") : \n\n\"" + obj.getString("text") + "\"";

        message.setText(text);
        emailSender.send(message);

    }

    public JavaMailSenderImpl getJavaMailSender() throws FileNotFoundException {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Scanner sc = new Scanner(new File("smtp.txt"));
        while(sc.hasNextLine()){
            String str = sc.nextLine();
            switch(str.split(":")[0]){
                case "host":
                    mailSender.setHost(AES.decrypt(str.split(":")[1]));
                    break;
                case "port":
                    mailSender.setPort(Integer.parseInt(AES.decrypt(str.split(":")[1])));
                    break;
                case "email":
                    mailSender.setUsername(AES.decrypt(str.split(":")[1]));
                    break;
                case "password":
                    mailSender.setPassword(AES.decrypt(str.split(":")[1]));
                    break;
                default:
                    // code block
            }
        }
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}