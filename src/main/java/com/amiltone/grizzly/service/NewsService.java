package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.NewsEntity;
import com.amiltone.grizzly.entity.TeamsEntity;
import com.amiltone.grizzly.holder.NewsHolder;
import com.amiltone.grizzly.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class NewsService {

    @Autowired
    private NewsRepository newsRepository;
    @Autowired
    private PictureService pictureService;

    public NewsEntity findNewsById (Long id) {
        return newsRepository.findById(id).orElse(null);
    }

    public List<NewsEntity> findAllNewsByDate () {
        return newsRepository.findAllNewsByDate();
    }

    public NewsHolder find3NewsByDate(int index){
        Boolean newsLeft = true;
        List<NewsEntity> listNews = newsRepository.find3NewsByDate(index);
        if((index+3)>=newsRepository.count()){
            newsLeft = false;
        }
        return new NewsHolder(listNews, newsLeft);

    }

    public NewsHolder find5NewsByDate(int index){
        Boolean newsLeft = true;
        List<NewsEntity> listNews = newsRepository.find5NewsByDate(index);
        if((index+5)>=newsRepository.count()){
            newsLeft = false;
        }
        return new NewsHolder(listNews, newsLeft);

    }


    public NewsHolder find20NewsByDate(int index){
        Boolean newsLeft = true;
        List<NewsEntity> listNews = newsRepository.find20NewsByDate(index);
        if((index+20)>=newsRepository.count()){
            newsLeft = false;
        }
        return new NewsHolder(listNews, newsLeft);

    }

    public List<NewsEntity> findAllNews () {
        return newsRepository.findAll();
    }

    public List<NewsEntity> findNewsByTitle(String title){
        return newsRepository.findNewsByTitle(title);
    }



    public NewsEntity updateNews (Long id, NewsEntity newsEntity ) {
        NewsEntity updatingNews = newsRepository.findById(id).orElse(null);
        System.out.println(newsEntity.getDate_news());
        updatingNews.setDate_news(newsEntity.getDate_news());
        updatingNews.setArticle_news(newsEntity.getArticle_news());
        updatingNews.setTeams(newsEntity.getTeams());
        updatingNews.setTitle_news(newsEntity.getTitle_news());
        updatingNews.setPicture(newsEntity.getPicture());

        if(updatingNews.getPicture().getName_picture().equals("")) updatingNews.setPicture(null);

        return newsRepository.save(updatingNews);
    }

    public NewsEntity saveNews (NewsEntity newsEntity) {
        if(newsEntity.getPicture().getName_picture().equals("")) newsEntity.setPicture(null);
        return newsRepository.save(newsEntity);
    }


    public String deleteNews (Long id){
        String title = this.findNewsById(id).getTitle_news();
        newsRepository.deleteById(id);
        return "l'actualité \""+title+"\" a été suprrimée.";
    }

    public void removeTeam(Long id){
        List<NewsEntity> newsList = this.findAllNews();
        newsList.forEach(
                newsEntity -> {
                    Collection<TeamsEntity> news_teams;
                    news_teams = newsEntity.getTeams();
                    news_teams.removeIf(t -> t.getId_team_teams()==id);
                    newsEntity.setTeams(news_teams);
                    updateNews(newsEntity.getId_news(), newsEntity);
                }
        );
    }
}
