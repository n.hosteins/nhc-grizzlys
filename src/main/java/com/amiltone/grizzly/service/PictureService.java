package com.amiltone.grizzly.service;

import com.amiltone.grizzly.GrizzlyUtils;
import com.amiltone.grizzly.entity.PictureEntity;
import com.amiltone.grizzly.repository.PictureRepository;
import com.amiltone.grizzly.storage.FilesStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.NoResultException;
import java.io.IOException;

@Service
public class PictureService {

    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private FilesStorageService storageService;

    public PictureEntity findPictureById (Integer id) {
        return pictureRepository.findById(id).orElse(null);
    }

    public PictureEntity addPicture (PictureEntity pictureEntity) { return pictureRepository.save(pictureEntity); }

    public PictureEntity updatePicture (int picture_id, PictureEntity pictureEntity) {
        PictureEntity updatePicture = pictureRepository.findById(picture_id).orElse(null);
        updatePicture.setName_picture(pictureEntity.getName_picture());
        updatePicture.setAlt_picture(pictureEntity.getAlt_picture());
        return  pictureRepository.save(updatePicture);
    }

    public String deletePictureById (Integer id) {
        pictureRepository.deleteById(id);
        return "picture id " + id + " deleted successfully";
    }

    public void replacePictureFile(final PictureEntity picture, MultipartFile file) throws IOException {
        if (picture == null) {
            return;
        }

        if (picture.getId_picture() != 0) {
            // l'entité existe déjà, mettre à jour
            final PictureEntity oldPicture = this.findPictureById(picture.getId_picture());
            if (oldPicture == null) {
                throw GrizzlyUtils.getPasDeBol();
            }
            final String oldFileName = oldPicture.getName_picture();
            // renomme le fichier
            storageService.updatePicture(oldFileName, picture.getName_picture());
            // met à jour l'éntité
            this.updatePicture(picture.getId_picture(), picture);
        }
        /*else if (file == null || file.isEmpty()) {
            throw GrizzlyUtils.getMissingImageException();
        }*/

        // sauvegarder le fichier
        if (file != null && !file.isEmpty()) {
            try {
                storageService.save(file, picture.getName_picture());
            } catch (RuntimeException e) {
                throw new IOException("Erreur pendant la sauvegarde du fichier", e);
            }
        }
    }

    public void checkPictureFile(PictureEntity pic) throws IOException{
        if(pic != null) {
            String pic_name = pic.getName_picture();
            int id = pic.getId_picture();
            if (id == 0) {
                if (pic_name != "") {
                    if (this.pictureExists(pic_name)) {
                        throw GrizzlyUtils.getPictureNameAlreadyUse(pic_name);
                    }
                }
            } else {
                if (!this.findPictureById(id).getName_picture().equals(pic_name)) {
                    if (this.pictureExists(pic_name)) {
                        throw GrizzlyUtils.getPictureNameAlreadyUse(pic_name);
                    }
                }
            }
        }
    }

    public void deletePictureFile(@Nullable PictureEntity pic) {
        if (pic == null) {
            return;
        }
        final String name = pic.getName_picture();
        if (StringUtils.isEmpty(name)) {
            return;
        }
        try {
            storageService.deletePicture(pic.getName_picture());
        } catch (Exception e) {
            throw new RuntimeException("Supression de l'image impossible");
        }
    }

    private boolean pictureExists(String name) {
        final PictureEntity picture = pictureRepository.findByName(name);
        return picture != null;
    }

}