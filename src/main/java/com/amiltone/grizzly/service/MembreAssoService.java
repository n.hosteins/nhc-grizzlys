package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.MembreAssoEntity;
import com.amiltone.grizzly.repository.MembreAssoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MembreAssoService {
    @Autowired
    private MembreAssoRepository membreAssoRepository;
    @Autowired
    private PictureService pictureService;

    public List<MembreAssoEntity> getAllMembreAsso () {return  membreAssoRepository.findAllSortByPriority();}

    public MembreAssoEntity getOneMembreAsso (Long id) { return  membreAssoRepository.findById(id).orElse(null);}

    public long count(){
        return membreAssoRepository.count();
    }

    public MembreAssoEntity addMembreAsso (MembreAssoEntity membreAssoEntity) {
        if(membreAssoEntity.getPicture().getName_picture().equals("")) membreAssoEntity.setPicture(null);
        return  membreAssoRepository.save(membreAssoEntity);
    }

    public  MembreAssoEntity updateMembreAsso (Long id , MembreAssoEntity membreAssoEntity) {
        MembreAssoEntity updatingMembreAsso = membreAssoRepository.findById(id).orElse(null);
        updatingMembreAsso.setFirstname_membre_asso(membreAssoEntity.getFirstname_membre_asso());
        updatingMembreAsso.setLastname_membre_asso(membreAssoEntity.getLastname_membre_asso());
        updatingMembreAsso.setMail_membre_asso(membreAssoEntity.getMail_membre_asso());
        updatingMembreAsso.setPhone_membre_asso(membreAssoEntity.getPhone_membre_asso());
        updatingMembreAsso.setDescription_membre_asso(membreAssoEntity.getDescription_membre_asso());
        updatingMembreAsso.setPicture(membreAssoEntity.getPicture());

        if(updatingMembreAsso.getPicture().getName_picture().equals("")) updatingMembreAsso.setPicture(null);
        return membreAssoRepository.save(updatingMembreAsso);
    }

    public String deleteMembreAsso (Long id) {
        MembreAssoEntity membre = this.getOneMembreAsso(id);
        membreAssoRepository.deleteById(id);
        return membre.getLastname_membre_asso()+" "+membre.getFirstname_membre_asso()+" a été suprrimé(e) de la liste des membres de l'association.";

    }
}