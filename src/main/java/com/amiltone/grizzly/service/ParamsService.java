package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.ParamsEntity;
import com.amiltone.grizzly.repository.ParamsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParamsService {

    @Autowired
    private ParamsRepository paramsRepository;

    public ParamsEntity findParamsById (Long id) { return  paramsRepository.findById(id).orElse(null);}

    public ParamsEntity updateParams (Long id , ParamsEntity paramsEntity) {
        ParamsEntity updatingInfo = paramsRepository.findById(id).orElse(null);
        if(paramsEntity.getMeta_text() != null) updatingInfo.setMeta_text(paramsEntity.getMeta_text());
        if(paramsEntity.getMeta_title() != null) updatingInfo.setMeta_title(paramsEntity.getMeta_title());

        if(paramsEntity.getEmail_contact() != null) updatingInfo.setEmail_contact(paramsEntity.getEmail_contact());
        if(paramsEntity.getAddress_asso() != null) updatingInfo.setAddress_asso(paramsEntity.getAddress_asso());
        if(paramsEntity.getPhone_asso() != null) updatingInfo.setPhone_asso(paramsEntity.getPhone_asso());
        if(paramsEntity.getPresident_asso() != null) updatingInfo.setPresident_asso(paramsEntity.getPresident_asso());

        if(paramsEntity.getPeriod_name() != null) updatingInfo.setPeriod_name(paramsEntity.getPeriod_name());

        if(paramsEntity.getHome_title() != null) updatingInfo.setHome_title(paramsEntity.getHome_title());
        if(paramsEntity.getHome_slogan() != null) updatingInfo.setHome_slogan(paramsEntity.getHome_slogan());
        if(paramsEntity.getHome_values_1() != null) updatingInfo.setHome_values_1(paramsEntity.getHome_values_1());
        if(paramsEntity.getHome_values_2() != null) updatingInfo.setHome_values_2(paramsEntity.getHome_values_2());
        if(paramsEntity.getHome_values_3() != null) updatingInfo.setHome_values_3(paramsEntity.getHome_values_3());
        if(paramsEntity.getHome_values_4() != null) updatingInfo.setHome_values_4(paramsEntity.getHome_values_4());

        if(paramsEntity.getDescription_nhc() != null) updatingInfo.setDescription_nhc(paramsEntity.getDescription_nhc());
        if(paramsEntity.getLegal_notice() != null) updatingInfo.setLegal_notice(paramsEntity.getLegal_notice());

        updatingInfo.setPicture(paramsEntity.getPicture());

        if(updatingInfo.getPicture().getName_picture().equals("")) updatingInfo.setPicture(null);

        return paramsRepository.save(updatingInfo);
    }
}
