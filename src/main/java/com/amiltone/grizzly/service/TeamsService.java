package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.TeamsEntity;
import com.amiltone.grizzly.repository.TeamsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamsService {

    @Autowired
    private TeamsRepository teamsRepository;

    /**
     * @return toutes les equipes de la base de données
     */
    public List<TeamsEntity> findAllTeams () {
        return teamsRepository.findAllSortByPriority();
    }

    /**
     *
     * @param id
     * @return toute les info d'une equipe
     */
    public  TeamsEntity findTeamById (Long id) {
        return teamsRepository.findById(id).orElse(null);
    }

    /**
     *
     * @param teamsEntity
     * @return ajotue une nouvelle equipe dans la basse de donnée
     */
    public TeamsEntity addTeam (TeamsEntity teamsEntity) {
        if(teamsEntity.getPicture().getName_picture().equals("")) teamsEntity.setPicture(null);
        if(teamsEntity.getPic_supervisor1().getName_picture().equals("")) teamsEntity.setPic_supervisor1(null);
        if(teamsEntity.getPic_supervisor2().getName_picture().equals("")) teamsEntity.setPic_supervisor2(null);
        if(teamsEntity.getPic_supervisor3().getName_picture().equals("")) teamsEntity.setPic_supervisor3(null);
        return teamsRepository.save(teamsEntity); }


    public long count(){
        return teamsRepository.count();
    }

    /**
     *
     * @param id
     * @param teamsEntity
     * @return mets a jour l'equipe en cascade image aussi
     */

    public  TeamsEntity updateTeam (Long id, TeamsEntity teamsEntity ){
        TeamsEntity updatingTeam = teamsRepository.findById(id).orElse(null);

        updatingTeam.setPicture(teamsEntity.getPicture());
        updatingTeam.setName_team(teamsEntity.getName_team());
        updatingTeam.setPresentation_team(teamsEntity.getPresentation_team());

        updatingTeam.setSupervisor_1(teamsEntity.getSupervisor_1());
        updatingTeam.setPic_supervisor1(teamsEntity.getPic_supervisor1());
        updatingTeam.setFunction_supervisor1(teamsEntity.getFunction_supervisor1());
        updatingTeam.setMail_supervisor1(teamsEntity.getMail_supervisor1());

        updatingTeam.setSupervisor_2(teamsEntity.getSupervisor_2());
        updatingTeam.setPic_supervisor2(teamsEntity.getPic_supervisor2());
        updatingTeam.setFunction_supervisor2(teamsEntity.getFunction_supervisor2());
        updatingTeam.setMail_supervisor2(teamsEntity.getMail_supervisor2());

        updatingTeam.setSupervisor_3(teamsEntity.getSupervisor_3());
        updatingTeam.setPic_supervisor3(teamsEntity.getPic_supervisor3());
        updatingTeam.setFunction_supervisor3(teamsEntity.getFunction_supervisor3());
        updatingTeam.setMail_supervisor3(teamsEntity.getMail_supervisor3());
        updatingTeam.setSupervisors(teamsEntity.getSupervisors());

        if(updatingTeam.getPicture().getName_picture().equals("")) updatingTeam.setPicture(null);
        if(updatingTeam.getPic_supervisor1().getName_picture().equals("")) updatingTeam.setPic_supervisor1(null);
        if(updatingTeam.getPic_supervisor2().getName_picture().equals("")) updatingTeam.setPic_supervisor2(null);
        if(updatingTeam.getPic_supervisor3().getName_picture().equals("")) updatingTeam.setPic_supervisor3(null);

        if(teamsEntity.getPriority() != null) updatingTeam.setPriority(teamsEntity.getPriority());

        return teamsRepository.save(updatingTeam);
    }


    /**
     *
     * @param id
     * suprimer l'equipe en cascade image aussi
     */
    public String deleteTeamById (Long id) {
        TeamsEntity team = this.findTeamById(id);
        teamsRepository.deleteById(id);
        return "L'équipe "+team.getName_team()+" a été supprimée.";
    }

}
