package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.TeamsEntity;
import com.amiltone.grizzly.entity.TrainingEntity;
import com.amiltone.grizzly.repository.TrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class TrainingService {
    @Autowired
    private TrainingRepository trainingRepository;

    public TrainingEntity findTrainingById (Long id){
        return trainingRepository.findById(id).orElse(null);
    }

    public List<TrainingEntity> findAllTraining () {
        return trainingRepository.findAll();
    }

    public List<TrainingEntity> findTrainingByPeriodDay(final String period, final String day) {
        return trainingRepository.findTrainingByPeriodDay(period.toLowerCase(), day.toLowerCase());
    }

    public String deleteTraining (Long id){
        TrainingEntity training = this.findTrainingById(id);
        trainingRepository.deleteById(id);
        return "L'entrainement du "+training.getDay_training()+" à "+training.getStart_hour_training()+" a bien été supprimé.";
    }


    public TrainingEntity updateTraining (TrainingEntity trainingEntity){
        return trainingRepository.save(trainingEntity);
    }

    public TrainingEntity saveTraining (TrainingEntity toAdd){
        return trainingRepository.save(toAdd); }
        
    public void removeTeam(Long id){
        List<TrainingEntity> trainingList = this.findAllTraining();
        trainingList.forEach(
                trainingEntity -> {
                    Collection<TeamsEntity> training_teams;
                    training_teams = trainingEntity.getTeams();
                    training_teams.removeIf(t -> t.getId_team_teams()==id);
                    trainingEntity.setTeamsEntities(training_teams);
                    updateTraining(trainingEntity);
                }
        );
    }
}
