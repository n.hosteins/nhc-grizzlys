package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.PartenaireEntity;
import com.amiltone.grizzly.repository.PartenaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartenaireService {
    @Autowired
    private PartenaireRepository partenaireRepository;
    @Autowired
    private PictureService pictureService;

    public List<PartenaireEntity> getAllPartenaires () {return  partenaireRepository.findAllSortByPriority();}

    public PartenaireEntity findPartenairesById (Long id) { return  partenaireRepository.findById(id).orElse(null);}

    public long count(){
        return partenaireRepository.count();
    }

    public PartenaireEntity addPartenaire (PartenaireEntity partenaireEntity) {
        if(partenaireEntity.getPicture().getName_picture().equals("")) partenaireEntity.setPicture(null);
        return  partenaireRepository.save(partenaireEntity);
    }

    public PartenaireEntity updatePartenaire (Long id , PartenaireEntity partenaireEntity) {
        PartenaireEntity updatingPartenaire = partenaireRepository.findById(id).orElse(null);
        updatingPartenaire.setName_partner(partenaireEntity.getName_partner());
        updatingPartenaire.setCity_partner(partenaireEntity.getCity_partner());
        updatingPartenaire.setDescription_partner(partenaireEntity.getDescription_partner());
        updatingPartenaire.setZip_partner(partenaireEntity.getZip_partner());
        updatingPartenaire.setMail_partner(partenaireEntity.getMail_partner());
        updatingPartenaire.setPhone_partner(partenaireEntity.getPhone_partner());
        updatingPartenaire.setStreet_partner(partenaireEntity.getStreet_partner());
        updatingPartenaire.setWebsite_partner(partenaireEntity.getWebsite_partner());
        updatingPartenaire.setInstitutional(partenaireEntity.getInstitutional());
        updatingPartenaire.setPicture(partenaireEntity.getPicture());

        if(updatingPartenaire.getPicture().getName_picture().equals("")) updatingPartenaire.setPicture(null);
        return partenaireRepository.save(updatingPartenaire);
    }

    public String deletePartenaire (Long id) {
        PartenaireEntity partner = this.findPartenairesById(id);
        partenaireRepository.deleteById(id);
        return "Le partenaire "+partner.getName_partner()+" a été supprimé.";
    }

}
