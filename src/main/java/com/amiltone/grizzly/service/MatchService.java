package com.amiltone.grizzly.service;

import com.amiltone.grizzly.entity.MatchEntity;
import com.amiltone.grizzly.entity.NewsEntity;
import com.amiltone.grizzly.entity.TeamsEntity;
import com.amiltone.grizzly.holder.MatchHolder;
import com.amiltone.grizzly.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class MatchService {

    @Autowired
    MatchRepository matchRepository;

    public MatchHolder find20Match(Long index){
        Boolean matchLeft = true;
        List<MatchEntity> listMatch = matchRepository.find20Match(index);
        if((index+20)>=matchRepository.count()){
            matchLeft = false;
        }

        return new MatchHolder(listMatch, matchLeft);
    }

    public List<MatchEntity> findAllMatchByDate () {
        return matchRepository.findAllMatchByDate();
    }

    public List<MatchEntity> findAllMatch () {
        return matchRepository.findAll();
    }

    public MatchEntity findMatchById (Long id) {
        return matchRepository.findById(id).orElse(null);
    }

    public List<MatchEntity> findMatchByIdTeam(Long id){
        return matchRepository.findMatchByIdTeam(id);
    }

    public List<MatchEntity> findMatchByDate(String date){
        return matchRepository.findMatchByDate(date);
    }


    public MatchEntity saveMatch (MatchEntity matchEntity) { return matchRepository.save(matchEntity); }

    public MatchEntity updateMatch (Long id, MatchEntity matchEntity ) {
        MatchEntity updatingMatch = matchRepository.findById(id).orElse(null);
        updatingMatch.setTeam(matchEntity.getTeam());
        updatingMatch.setTitle(matchEntity.getTitle());
        updatingMatch.setLocation(matchEntity.getLocation());
        updatingMatch.setHour_match(matchEntity.getHour_match());
        updatingMatch.setDate_match(matchEntity.getDate_match());
        return matchRepository.save(updatingMatch);
    }

    public String deleteMatch (Long id){
        MatchEntity match = this.findMatchById(id);
        matchRepository.deleteById(id);
        return "Le match "+match.getTitle()+" du "+match.getDate_match()+" a été supprimé.";
    }

    public void removeMatchTeam(Long id){
        List<MatchEntity> matchList = this.findAllMatch();
        matchList.forEach(
            matchEntity -> {
                if(matchEntity.getTeam().getId_team_teams()==id) {
                    deleteMatch(matchEntity.getId_match());
                }
            }
        );
    }
}
