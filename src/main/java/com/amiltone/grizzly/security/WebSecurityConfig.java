package com.amiltone.grizzly.security;

import com.amiltone.grizzly.response.ResponseStatus;
import com.amiltone.grizzly.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Fichier de configuration de spring web security
 * il va traiter de l'authentification et de la manière dont seront traitées les requêtes
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Autoconstruction de l'objet UserService
     */
    @Autowired
    private UserService userDetailsService;

    /**
     * Gestion de l'authentification en passant par l'outil AuthenticationManagerBuilder
     * @param auth paramètre de type AuthenticationManagerBuilder
     * @throws Exception si l'autentification ne fonctionne pas
     */
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    /**
     * Gestion du trigger de web security par rapport aux chemins de l'API, à changer.
     * @param http paramètre de type HttpSecurity
     * @throws Exception si les requêtes n'aboutissent à rien
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/gestion/static/**")
                .permitAll()
                .antMatchers("/gestion/**")
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/connexion")
                .defaultSuccessUrl("/gestion/accueil", true)
                .failureUrl("/connexion")
                .successHandler(new AuthenticationSuccessHandler() {
                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
                        final PrintWriter out = httpServletResponse.getWriter();
                        final ResponseStatus responseStatus = new ResponseStatus( "success","Connexion avec succes ");
                        objectMapper.writeValue(out, responseStatus);

                    }
                })
                .failureHandler(new AuthenticationFailureHandler() {
                    @Override
                    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
                        final PrintWriter out = httpServletResponse.getWriter();
                        final ResponseStatus responseStatus = new ResponseStatus( "error","Le mot de pass ou l'identifiant sont incorrect ");
                        objectMapper.writeValue(out, responseStatus);
                    }
                });
    }

    /**
     * Bean permettant l'encodage de password
     * @return l'encodeur de password
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }



}
