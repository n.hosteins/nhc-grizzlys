package com.amiltone.grizzly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class GrizzlyUtils {
	public static RuntimeException getPasDeBol() {
		return new IllegalStateException("Erreur inconnue");
	}

	public static GrizzlyException getPictureNameAlreadyUse(final String filename) {
		return new GrizzlyException("Le nom de l'image est déjà utilisé : " + filename);
	}
	public static GrizzlyException getMissingImageException() {
		return new GrizzlyException("Il manque une image");
	}

	private GrizzlyUtils() {

	}
}
