package com.amiltone.grizzly.holder;

import com.amiltone.grizzly.entity.TrainingEntity;

import java.util.List;

public class DayHolder {
    String day;
    List<TrainingEntity> trainings;

    public DayHolder(List<TrainingEntity> trainings, String day) {
        this.day = day;
        this.trainings = trainings;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<TrainingEntity> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<TrainingEntity> trainings) {
        this.trainings = trainings;
    }
}
