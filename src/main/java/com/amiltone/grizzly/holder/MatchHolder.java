package com.amiltone.grizzly.holder;

import com.amiltone.grizzly.entity.MatchEntity;

import java.util.List;

public class MatchHolder {
    private List<MatchEntity> matchEntities;
    private Boolean matchLeft;

    public MatchHolder(List<MatchEntity> matchEntities, Boolean matchLeft) {
        this.matchEntities = matchEntities;
        this.matchLeft = matchLeft;
    }

    public List<MatchEntity> getMatchEntities() {
        return matchEntities;
    }

    public void setMatchEntities(List<MatchEntity> matchEntities) {
        this.matchEntities = matchEntities;
    }

    public Boolean getMatchLeft() {
        return matchLeft;
    }

    public void setMatchLeft(Boolean matchLeft) {
        this.matchLeft = matchLeft;
    }
}
