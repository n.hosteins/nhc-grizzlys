package com.amiltone.grizzly.holder;

import java.util.ArrayList;
import java.util.List;

public class TrainingHolder {


    private List<DayHolder> days = new ArrayList<DayHolder>();

    public TrainingHolder() {
    }

    public List<DayHolder> getDays() {
        return days;
    }

    public void setDays(List<DayHolder> days) {
        this.days = days;
    }
}
