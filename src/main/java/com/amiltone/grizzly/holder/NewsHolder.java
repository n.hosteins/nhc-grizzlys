package com.amiltone.grizzly.holder;

import com.amiltone.grizzly.entity.NewsEntity;

import java.util.List;

public class NewsHolder {
    private List<NewsEntity> news;
    private Boolean newsLeft;

    public List<NewsEntity> getNews() {
        return news;
    }

    public void setNews(List<NewsEntity> news) {
        this.news = news;
    }


    public Boolean getNewsLeft() {
        return newsLeft;
    }

    public void setNewsLeft(Boolean newsLeft) {
        this.newsLeft = newsLeft;
    }

    public NewsHolder(List<NewsEntity> news, Boolean newsLeft) {
        this.news = news;
        this.newsLeft = newsLeft;
    }
}
